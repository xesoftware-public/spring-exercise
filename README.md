https://github.com/RiskNarrative/spring-exercise

- A request parameter has to be added to decide whether only active `companies` should be returned

Should be

-  A request parameter has to be added to decide whether only active `officers` should be returned

I was not provided with an api key so just relied on the documented json, 
so I did not call https://exercise.trunarrative.cloud/TruProxyAPI/rest/Companies/v1

- As there was no documentation on what getting a company returned e.g. gathered from the self links..
  https://exercise.trunarrative.cloud/TruProxyAPI/rest/Companies/v1/company/06500244
  ...and no api key, I have assumed that Search?Query={search_term} where the {search_term} is the company id will return the expected company entity.
- The search api endpoint I have implemented is a POST as it takes a json body.
  It probably should be a GET (as search doesn't update) but conflicts with spring boot testing which doesn't support GET with a body. Can override this behavior but as it wasn't explicitly defined whether it is to be a GET or POST, so just made it a POST.

- Added simple unit tests with mockito.
- Added simple integration tests with wiremock.
- Generated dtos with https://www.jsonschema2pojo.org/