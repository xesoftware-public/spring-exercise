package com.risknarrative.springexercise;

import com.risknarrative.springexercise.company.CompaniesService;
import com.risknarrative.springexercise.dto.Item;
import com.risknarrative.springexercise.dto.Officer;
import com.risknarrative.springexercise.dto.Request;
import com.risknarrative.springexercise.dto.Response;
import com.risknarrative.springexercise.officer.OfficersService;
import lombok.NonNull;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class SearchService {

    final CompaniesService companiesService;
    final OfficersService officersService;

    public SearchService(final CompaniesService companiesService, final OfficersService officersService) {
        this.companiesService = companiesService;
        this.officersService = officersService;
    }

    public ResponseEntity<Response> search(final String xApiKey, @NonNull final Request request, @NonNull final Boolean active) {
        ResponseEntity<Response> ret = null;
        if (StringUtils.hasText(xApiKey)) {
            if (StringUtils.hasText(request.getCompanyNumber())) {
                // assume can get company by searching for company id and it will return only one result!
                // alternatively could use https://exercise.trunarrative.cloud/TruProxyAPI/rest/Companies/v1/company/{companyNumber}
                // but that isn't documented anywhere!
                ret = buildResponseEntity(xApiKey, request.getCompanyNumber(), active);
            } else if (StringUtils.hasText(request.getCompanyName())) {
                ret = buildResponseEntity(xApiKey, request.getCompanyName(), active);
            } else {
                ret = ResponseEntity.badRequest().build();
            }
        } else {
            ret = ResponseEntity.badRequest().build();
        }

        return ret;
    }

    private ResponseEntity<Response> buildResponseEntity(final String xApiKey, @NonNull final String company, boolean active) {
        final Response response = new Response();
        com.risknarrative.springexercise.company.dto.Response companyResponse = companiesService.search(company);
        if (null == companyResponse || companyResponse.totalResults == null || 0 == companyResponse.totalResults)
            return ResponseEntity.notFound().build();

        response.items = companyResponse.items.stream().map(item -> buildResponseItem(item, active)).toList();
        response.totalResults = response.items.size();
        return ResponseEntity.ok(response);
    }

    private Item buildResponseItem(@NonNull final com.risknarrative.springexercise.company.dto.Item item, boolean active) {
        String companyNumber = item.companyNumber;
        com.risknarrative.springexercise.officer.dto.Response officerResponse = officersService.findOfficers(companyNumber);

        return buildResponseItem(item, officerResponse, active);
    }

    private static Item buildResponseItem(@NonNull final com.risknarrative.springexercise.company.dto.Item item, @NonNull com.risknarrative.springexercise.officer.dto.Response officerResponse, boolean active) {
        final Item ret =  new Item();
        ret.companyNumber = item.companyNumber;
        ret.address = item.address;
        ret.companyStatus = item.companyStatus;
        ret.companyType = item.companyType;
        ret.title = item.title;
        ret.dateOfCreation = item.dateOfCreation;
        // build officers for each company.
        ret.officers = officerResponse.items.stream()
                .filter(officer -> !active || null == officer.resignedOn)
                .map(SearchService::buildResponseOfficer).toList();

        return ret;
    }

    private static Officer buildResponseOfficer(com.risknarrative.springexercise.officer.dto.Item item) {
        final Officer ret =  new Officer();
        ret.address = item.address;
        ret.name = item.name;
        ret.officerRole = item.officerRole;
        ret.appointedOn = item.appointedOn;
        return ret;
    }
}
