package com.risknarrative.springexercise;

import com.risknarrative.springexercise.dto.Request;
import com.risknarrative.springexercise.dto.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api")
public class APIController {

    final SearchService searchService;

    public APIController(SearchService searchService) {
        this.searchService = searchService;
    }

    @PostMapping
    public ResponseEntity<Response> search(@RequestHeader("x-api-key") final String xApiKey, @RequestBody Request request, @RequestParam Boolean active) {
        return this.searchService.search(xApiKey, request, active);
    }
}
