package com.risknarrative.springexercise.officer.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.processing.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("jsonschema2pojo")
public class Links {

    @JsonProperty("self")
    public String self;
    @JsonProperty("officer")
    public Officer officer;
}
