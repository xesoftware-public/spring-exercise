package com.risknarrative.springexercise.officer.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.risknarrative.springexercise.dto.Address;

import javax.annotation.processing.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "address",
        "name",
        "appointed_on",
        "resigned_on",
        "officer_role",
        "links",
        "date_of_birth",
        "occupation",
        "country_of_residence",
        "nationality"
})
@Generated("jsonschema2pojo")
public class Item {

    @JsonProperty("address")
    public Address address;
    @JsonProperty("name")
    public String name;
    @JsonProperty("appointed_on")
    public String appointedOn;
    @JsonProperty("resigned_on")
    public String resignedOn;
    @JsonProperty("officer_role")
    public String officerRole;
    @JsonProperty("links")
    public Links links;
    @JsonProperty("date_of_birth")
    public DateOfBirth dateOfBirth;
    @JsonProperty("occupation")
    public String occupation;
    @JsonProperty("country_of_residence")
    public String countryOfResidence;
    @JsonProperty("nationality")
    public String nationality;
}
