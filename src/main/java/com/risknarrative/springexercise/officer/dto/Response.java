package com.risknarrative.springexercise.officer.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.processing.Generated;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "etag",
        "links",
        "kind",
        "items_per_page",
        "items"
})
@Generated("jsonschema2pojo")
public class Response {

    @JsonProperty("etag")
    public String etag;
    @JsonProperty("links")
    public Links links;
    @JsonProperty("kind")
    public String kind;
    @JsonProperty("items_per_page")
    public Integer itemsPerPage;
    @JsonProperty("items")
    public List<Item> items;

}
