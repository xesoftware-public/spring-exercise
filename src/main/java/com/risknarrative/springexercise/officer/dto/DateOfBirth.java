package com.risknarrative.springexercise.officer.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.processing.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "month",
        "year"
})
@Generated("jsonschema2pojo")
public class DateOfBirth {
    @JsonProperty("month")
    public Integer month;
    @JsonProperty("year")
    public Integer year;
}
