package com.risknarrative.springexercise.officer;

import com.risknarrative.springexercise.officer.dto.Response;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
public class OfficersService {
    @Value("${companies.url}")
    private String url;

    final RestTemplate restTemplate;

    public OfficersService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Response findOfficers(@NonNull final String companyNumber) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        final HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
        try {
            final ResponseEntity<Response> result =
                    restTemplate.exchange(url + "/Officers?CompanyNumber=" + companyNumber, HttpMethod.GET, entity, Response.class);
            return result.getBody();
        } catch (final RestClientResponseException ignore) {
            return null;
        }
    }
}
