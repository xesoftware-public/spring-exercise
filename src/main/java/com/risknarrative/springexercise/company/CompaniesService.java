package com.risknarrative.springexercise.company;

import com.risknarrative.springexercise.company.dto.Response;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
public class CompaniesService {
    @Value("${companies.url}")
    private String url;

    final private RestTemplate restTemplate;

    public CompaniesService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Response search(@NonNull final String searchTxt) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        final HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
        try {
            final ResponseEntity<Response> result =
                    restTemplate.exchange(url + "/Search?Query=" + searchTxt, HttpMethod.GET, entity, Response.class);

            return result.getBody();
        } catch (final RestClientResponseException ignore) {
            return null;
        }
    }
}
