package com.risknarrative.springexercise.company.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.risknarrative.springexercise.dto.Address;

import javax.annotation.processing.Generated;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "company_status",
        "address_snippet",
        "date_of_creation",
        "matches",
        "description",
        "links",
        "company_number",
        "title",
        "company_type",
        "address",
        "kind",
        "description_identifier"
})
@Generated("jsonschema2pojo")
public class Item {

    @JsonProperty("company_status")
    public String companyStatus;
    @JsonProperty("address_snippet")
    public String addressSnippet;
    @JsonProperty("date_of_creation")
    public String dateOfCreation;
    @JsonProperty("matches")
    public Matches matches;
    @JsonProperty("description")
    public String description;
    @JsonProperty("links")
    public Links links;
    @JsonProperty("company_number")
    public String companyNumber;
    @JsonProperty("title")
    public String title;
    @JsonProperty("company_type")
    public String companyType;
    @JsonProperty("address")
    public Address address;
    @JsonProperty("kind")
    public String kind;
    @JsonProperty("description_identifier")
    public List<String> descriptionIdentifier;

}
