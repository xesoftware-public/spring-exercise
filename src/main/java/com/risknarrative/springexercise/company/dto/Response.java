package com.risknarrative.springexercise.company.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.processing.Generated;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "page_number",
        "kind",
        "total_results",
        "items"
})
@Generated("jsonschema2pojo")
public class Response {
    @JsonProperty("page_number")
    public Integer pageNumber;
    @JsonProperty("kind")
    public String kind;
    @JsonProperty("total_results")
    public Integer totalResults;
    @JsonProperty("items")
    public List<Item> items;
}
