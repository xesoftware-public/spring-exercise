
package com.risknarrative.springexercise.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.processing.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("jsonschema2pojo")
public class Address {
    @JsonProperty("locality")
    public String locality;
    @JsonProperty("postal_code")
    public String postalCode;
    @JsonProperty("premises")
    public String premises;
    @JsonProperty("address_line_1")
    public String addressLine1;
    @JsonProperty("country")
    public String country;
}
