package com.risknarrative.springexercise.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {
    @JsonProperty("total_results")
    public Integer totalResults;
    @JsonProperty("items")
    public List<Item> items;
}
