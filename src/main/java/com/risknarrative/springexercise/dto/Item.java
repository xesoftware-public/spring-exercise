package com.risknarrative.springexercise.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.processing.Generated;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("jsonschema2pojo")
public class Item {
    @JsonProperty("company_number")
    public String companyNumber;
    @JsonProperty("company_type")
    public String companyType;
    @JsonProperty("title")
    public String title;
    @JsonProperty("company_status")
    public String companyStatus;
    @JsonProperty("date_of_creation")
    public String dateOfCreation;
    @JsonProperty("address")
    public Address address;
    @JsonProperty("officers")
    public List<Officer> officers;
}
