package com.risknarrative.springexercise.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.processing.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("jsonschema2pojo")
public class Officer {
    @JsonProperty("name")
    public String name;
    @JsonProperty("officer_role")
    public String officerRole;
    @JsonProperty("appointed_on")
    public String appointedOn;
    @JsonProperty("address")
    public Address address;
}
