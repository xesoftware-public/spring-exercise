package com.risknarrative.springexercise.company;

import com.risknarrative.springexercise.company.dto.Response;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CompaniesServiceTest {

    @Mock
    private RestTemplate mockRestTemplate;

    @Test
    void search() {
        final CompaniesService companiesService = new CompaniesService(mockRestTemplate);
        final Response expectedResponse = new Response();
        expectedResponse.kind = "SomeKind";
        final ResponseEntity<Response> responseEntity = new ResponseEntity<>(expectedResponse, HttpStatus.OK);
        when(mockRestTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(Response.class)))
                .thenReturn(responseEntity);

       final Response response = companiesService.search("TEST1");

       assertNotNull(response);
       assertEquals(expectedResponse, response);
       assertEquals("SomeKind", response.kind);
    }
}